#include "Shape.h"

Shape::Shape(float area, float perimeter) //: _area(0), _perimeter(0)
{
	_area = area;
	_perimeter = perimeter;
}

float Shape::get_area() const
{
	return _area;
}
